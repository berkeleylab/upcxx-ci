# UPC++ CI Repository

This repository contains CI scripts for use by the maintainers of UPC++.  
You are probably looking for the main [UPC++ site](https://upcxx.lbl.gov).

If you *are* in the right place, then you probably want to read
[gitlab-ci.md](gitlab-ci.md) and/or [dev-ci/README.md](dev-ci/README.md)
instead of this file.
