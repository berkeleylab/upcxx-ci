## UPC++ GitLab CI configuration file
#
# - IMPORTANT NOTICES
#   - DO NOT: push any untrusted code
#     - Code (including shell code in configure and Makefiles) are run on
#       at least some "runners" as a normal user without the benefit of
#       containerization such as Docker.
#     - On Theta, CI jobs can run batch jobs charged against our allocation.
#   - DO: push only to your "user/" namespace.
#     - Assuming use of a remote named 'socks' and a Bitbucket user AnnExample:
#       `git push socks HEAD:annexample/check/issue-9876`
#
# See gitlab-ci.md for the main documentation

#
# Global configuration / settings
#
stages:
  - build
variables:
  CI_COMPILER: ""          # gnu, llvm, pgi, nvhpc, intel, cray, xcode, aocc
  CI_AGE: ""               # new, old
  CI_CPU: ""               # x86_64, knl, ppc64le
  CI_OS: ""                # linux, cnl, macos
  CI_HOST: ""              # dirac, power9, wombat, ...
  CI_NAME: ""              # name of a specific job, such as "dirac-pgi"
  CI_GROUP: ""             # name of a pre-defined group, such as "dirac-pgi_all"
  CI_NETWORKS: "smp udp"   # space- or comma-separated list of networks (conduits)
  CI_RANKS: 4
  CI_MAKE: make
  CI_MAKE_PARALLEL: ""
  CI_DEV_CHECK: ""
  CI_SNIPPET: 0
  CI_SINGLE: ""
  CI_EXTRAFLAGS: "-Werror"
  CI_UPCXX_CI_REPO: "https://bitbucket.org/berkeleylab/upcxx-ci.git"
  CI_UPCXX_CI_BRANCH:  "master"
  CI_KEEP: 0
  CI_SHELL: ""
  CI_GASNET_SEQ: 0
  CI_GASNET_PAR: 0
  CI_GASNET_PARSYNC: 0
  CI_GASNET_CONDUITS: ""

#
# Dummy job for defining YAML anchors to be used elsewhere
#
.defs:
  rules:
    # EXCLUDES based on CI_[FOO] set but not matching MY_[FOO]
    # TODO: merge to one complex expression?
    - &when_cc
      { if: '$CI_COMPILER != "" && $CI_COMPILER != $MY_COMPILER', when: never }
    - &when_age
      { if: '$CI_AGE      != "" && $CI_AGE      != $MY_AGE', when: never }
    - &when_os
      { if: '$CI_OS       != "" && $CI_OS       != $MY_OS', when: never }
    - &when_cpu
      { if: '$CI_CPU      != "" && $CI_CPU      != $MY_CPU', when: never }
    - &when_host
      { if: '$CI_HOST     != "" && $CI_HOST     != $MY_HOST', when: never }
    - &when_name
      { if: '$CI_NAME     != "" && $CI_NAME     != $CI_JOB_NAME', when: never }
    - &when_server
      { if: '$CI_SERVER_HOST != $MY_SERVER', when: never }
    - &when_web
      { if: '$CI_PIPELINE_SOURCE == "web"', when: always }
    - &when_push1
      if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME =~ /\/(dev-)?check\//'
      when: always
    - &when_push2
      if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "develop"'
      when: always
  variables: &old_gnu   { MY_COMPILER: 'gnu',   MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_gnu   { MY_COMPILER: 'gnu',   MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_llvm  { MY_COMPILER: 'llvm',  MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_llvm  { MY_COMPILER: 'llvm',  MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_intel { MY_COMPILER: 'intel', MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_intel { MY_COMPILER: 'intel', MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_oneapi { MY_COMPILER: 'oneapi', MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_oneapi { MY_COMPILER: 'oneapi', MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_pgi   { MY_COMPILER: 'pgi',   MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_pgi   { MY_COMPILER: 'pgi',   MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_nvhpc { MY_COMPILER: 'nvhpc', MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_nvhpc { MY_COMPILER: 'nvhpc', MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_cray  { MY_COMPILER: 'cray',  MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_cray  { MY_COMPILER: 'cray',  MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  variables: &old_xcode { MY_COMPILER: 'xcode', MY_AGE: 'old', UPCXX_HAVE_OPENMP: '', UPCXX_HAVE_CXX17: '' }
  variables: &new_xcode { MY_COMPILER: 'xcode', MY_AGE: 'new', UPCXX_HAVE_OPENMP: '', UPCXX_HAVE_CXX17: 1 }
  variables: &old_aocc  { MY_COMPILER: 'aocc',  MY_AGE: 'old', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: '' }
  variables: &new_aocc  { MY_COMPILER: 'aocc',  MY_AGE: 'new', UPCXX_HAVE_OPENMP: 1, UPCXX_HAVE_CXX17: 1 }
  script:
    - &do_ci_dev_check |-
      # Establish default value of $CI_DEV_CHECK
      if [[ -z "$CI_DEV_CHECK" ]]; then
        if [[ $CI_COMMIT_REF_NAME =~ /check/ ]]; then
          # CI_DEV_CHECK=0 due to branch name
          export CI_DEV_CHECK=0
        else
          # CI_DEV_CHECK=1 default
          export CI_DEV_CHECK=1
        fi
      else
        : # CI_DEV_CHECK was explicitly specified
      fi
    - &start_logging
      exec &> >(tee -a output.txt)
    - &do_ci_vars |-
      # Reporting and re-exporting of $CI_* variables
      # Should follow do_ci_dev_check
      CI_RUN_TESTS="${CI_RUN_TESTS:-1}"
      echo "==== CI SYSTEM VARS ===="
      for var in CI_JOB_URL CI_JOB_NAME CI_COMMIT_REF_NAME CI_COMMIT_SHA CI_PIPELINE_SOURCE\
                 CI_PIPELINE_ID CI_JOB_ID\
                 MY_HOST MY_OS MY_CPU MY_COMPILER MY_AGE ; do
        printf "%20s = '%s'\n" $var "${!var}"
      done
      echo "==== CI SELECTION VARS ===="
      for var in CI_COMPILER CI_AGE CI_CPU CI_OS CI_HOST CI_NAME; do
        printf "%20s = '%s'\n" $var "${!var}"
      done
      echo "==== CI CONTROL VARS ===="
      for var in CI_NETWORKS CI_RANKS CI_MAKE CI_MAKE_PARALLEL CI_DEV_CHECK CI_EXTRAFLAGS \
                 CI_RUN_TESTS CI_CONFIGURE_ARGS CI_TESTS CI_NO_TESTS CI_KEY CI_KEEP CI_SHELL \
                 CI_UPCXX_CI_REPO CI_UPCXX_CI_BRANCH CI_SINGLE CI_SNIPPET \
                 CI_GASNET_SEQ CI_GASNET_PAR CI_GASNET_PARSYNC CI_GASNET_CONDUITS; do
        printf "%20s = '%s'\n" $var "${!var}"
      done
      echo "===="
      export CONFIGURE_ARGS+=" --with-default-network=${CI_NETWORKS%%[, ]*}"
      export RANKS="$CI_RANKS"
      export TEST_CERR_LIMIT=${TEST_CERR_LIMIT:-50}
      if [[ -n "${MORE_NO_TESTS}" ]]; then
        export CI_NO_TESTS="${MORE_NO_TESTS}${CI_NO_TESTS:+,$CI_NO_TESTS}"
      fi
      export GASNET_BACKTRACE=${GASNET_BACKTRACE:-1}
      unset BITBUCKET_USER BITBUCKET_APP_PASSWORD
      env > environment
      DO_MAKE="$CI_MAKE $CI_MAKE_PARALLEL"  # NEVER export 'MAKE'
    - &post_build_status |-
      # Report build status of $STATE to Bitbucket if configured.
      # Must follow use of &do_ci_dev_check ($CI_DEV_CHECK used in build "key")
      # Must precede use, if any, of &do_ci_vars (which clears the credentials)
      if [[ -n "$BITBUCKET_APP_PASSWORD" ]]; then
        prevstate="$(set +o)"; set +x
        if [[ -z $BITBUCKET_REPO ]] ; then
          if [[ $CI_COMMIT_REF_NAME =~ ^([^/]*)/ ]]; then
            user="${BASH_REMATCH[1]}"
            BITBUCKET_REPO="${user}/"
            case $user in
              jdbachan) BITBUCKET_REPO+="upcxx-jdbachan";;
              *) BITBUCKET_REPO+=upcxx;;
            esac
          else
             BITBUCKET_REPO=berkeleylab/upcxx
          fi
        fi
        URL="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO}/commit/${CI_COMMIT_SHA}/statuses/build"
        if [[ -z ${CI_KEY} ]]; then
          if [[ -n "${CI_TESTS}${CI_NO_TESTS}" ]]; then
            CI_KEY='custom'
          elif (( ${CI_DEV_CHECK} )); then
            CI_KEY='dev-check'
          else
            CI_KEY='check'
          fi
        fi
        KEY="$CI_JOB_NAME.$CI_KEY"
        HREF="$CI_JOB_URL"
        if [[ -e output.txt && $CI_SNIPPET = 1 ]]; then
          TITLE="Job ${CI_JOB_ID}: commit ${CI_COMMIT_SHORT_SHA} on $KEY, ref ${CI_COMMIT_REF_NAME}."
          if [[ $STATE == 'FAILED' ]]; then
            TITLE+=' (FAILED)'
          fi
          curl -sS -u "$BITBUCKET_USER:$BITBUCKET_APP_PASSWORD" \
               https://api.bitbucket.org/2.0/snippets  \
               -F title="$TITLE" \
               -F file=@output.txt >snippet.json || true
          id="$(grep -o '"id":"[^"]*"' snippet.json | cut '-d"' -f4 || true)"
          if [[ -n "$id" ]]; then
            HREF="https://bitbucket.org/${BITBUCKET_USER}/workspace/snippets/${id}"
          fi
        fi
        DATA="{ \"state\": \"$STATE\", \"key\": \"$KEY\", \"url\": \"$HREF\" }"
        echo "DATA: $DATA"
        echo "URL:  $URL"
        curl -sS -u "$BITBUCKET_USER:$BITBUCKET_APP_PASSWORD" "$URL" \
           -X POST -H "Content-Type: application/json" -d "$DATA" 2>&1
        echo
        unset BITBUCKET_USER BITBUCKET_APP_PASSWORD
        eval "$prevstate"
      else
        echo "No BB status posted due to lack of BITBUCKET_APP_PASSWORD"
      fi
    - &do_gasnet_tests |-
      ( set -e
        CONDUIT_TMODE=""
        (( $CI_GASNET_SEQ ))     && CONDUIT_TMODE+=" seq"
        (( $CI_GASNET_PAR ))     && CONDUIT_TMODE+=" par"
        (( $CI_GASNET_PARSYNC )) && CONDUIT_TMODE+=" parsync"
        for net in ${CI_GASNET_CONDUITS:-${CI_NETWORKS}}; do
          for cmode in ${CI_SINGLE:-debug opt}; do
            makecmd="$CI_MAKE gasnet NETWORK=$net UPCXX_CODEMODE=$cmode"
            for tmode in $CONDUIT_TMODE; do
              $makecmd $CI_MAKE_PARALLEL -k DO_WHAT=tests-$tmode | cat
              if (( ${PIPESTATUS[0]} )); then
                echo FAILED to compile GASNet $tmode/$cmode/$net tests
                rm -f .pipe-passed
              else
                echo SUCCESS compiling GASNet $tmode/$cmode/$net tests
              fi
              [[ ,$DEV_CI_STAGES, =~ ,run, ]] || continue
              $makecmd DO_WHAT=run-tests 2>&1 | tee tmpfile
              if (( ${PIPESTATUS[0]} )) || grep -q '\*-\*' tmpfile; then
                echo FAILED to run GASNet $tmode/$cmode/$net tests
                rm -f .pipe-passed
              else
                echo SUCCESS running GASNet $tmode/$cmode/$net tests
              fi
            done
          done
        done
      ) || rm -f .pipe-passed

#
# Compiler templates (use with extends)
#
.old_gnu:   { variables: *old_gnu }
.new_gnu:   { variables: *new_gnu }
.old_llvm:  { variables: *old_llvm }
.new_llvm:  { variables: *new_llvm }
.old_intel: { variables: *old_intel }
.new_intel: { variables: *new_intel }
.old_oneapi: { variables: *old_oneapi }
.new_oneapi: { variables: *new_oneapi }
.old_pgi:   { variables: *old_pgi }
.new_pgi:   { variables: *new_pgi }
.old_nvhpc: { variables: *old_nvhpc }
.new_nvhpc: { variables: *new_nvhpc }
.old_cray:  { variables: *old_cray }
.new_cray:  { variables: *new_cray }
.old_xcode: { variables: *old_xcode }
.new_xcode: { variables: *new_xcode }
.old_aocc:  { variables: *old_aocc }
.new_aocc:  { variables: *new_aocc }

#
# Server templates (use with extends)
#
.server_socks:   { variables: { MY_SERVER: 'socks.lbl.gov', MY_REPO: 'Pagoda/upcxx' } }

#
# Rules templates (use with extends)
#
.rules_auto:
  rules:
    - { if: '$CI_GROUP != ""', when: never }
    - *when_server
    - *when_cc
    - *when_age
    - *when_os
    - *when_cpu
    - *when_host
    - *when_name
    - *when_web
    - *when_push1
    - *when_push2
.rules_manual:
  rules:
    - { if: '$CI_GROUP != ""', when: never }
    - *when_server
    - *when_cc
    - *when_age
    - *when_os
    - *when_cpu
    - { if: '$CI_HOST == $MY_HOST', when: always }
    - { if: '$CI_NAME == $CI_JOB_NAME', when: always }
    - when: never
.rules_group:
  rules:
    - *when_server
    - { if: '$CI_GROUP != "" && $CI_GROUP == $MY_GROUP', when: always }
    - { if: '$CI_GROUP != "" && $CI_GROUP == $MY_GROUP_2', when: always }
    - { if: '$CI_GROUP != "" && $CI_GROUP == $MY_GROUP_3', when: always }
    - { if: '$CI_GROUP != "" && $CI_GROUP == $MY_GROUP_4', when: always }
    - { if: '$CI_NAME == $CI_JOB_NAME', when: always }
    - when: never

#
# Job templates (use with extends)
#

# ".dev_ci" is a single-stage pipeline
# All the hard work is done using dev-ci scripts
.dev_ci:
  extends: [ .rules_auto ]
  stage: build
  artifacts:
    name: "gitlab-ci.$CI_COMMIT_SHORT_SHA.$CI_PIPELINE_ID.$CI_JOB_NAME"
    paths:
      - Makefile
      - bootstrap.log
      - config.log
      - environment
      - bld/gasnet.*/config.*
      - test-results/*/*.out
    reports: { junit: ./test-results/*/*.xml }
    when: always
  variables:
    ci_script: generic
    UPCXX_HAVE_STATIC_LIBS_CCS_SMP: 1  # execptional platforms must override
    UPCXX_HAVE_STATIC_LIBS_CCS_UDP: 1  # execptional platforms must override
  script:
    - *do_ci_dev_check
    - STATE=INPROGRESS
    - *post_build_status
    - *start_logging
    - *do_ci_vars
    - |-
      if (( $CI_RUN_TESTS )); then
        DEV_CI_STAGES="${DEV_CI_STAGES:-configure,build,tests,run}"
      else
        DEV_CI_STAGES="${DEV_CI_STAGES:-configure,build,tests}"
      fi
      echo "Fetching $CI_UPCXX_CI_REPO (branch $CI_UPCXX_CI_BRANCH) for use of dev-ci scripts"
      for ((i=0; i<5; ++i)); do
        git clone --branch $CI_UPCXX_CI_BRANCH --depth 1 $CI_UPCXX_CI_REPO upcxx-ci && break
        sleep 60
      done
      ( cd upcxx-ci && git log -n1 --oneline --no-decorate || true )
      ./upcxx-ci/dev-ci/$ci_script --stages="$DEV_CI_STAGES" --dir=. && touch .pipe-passed
    - *do_gasnet_tests
    - test -f .pipe-passed # determines overall success or failure of script
  after_script:
    - rm -rf test-*-*
    - *do_ci_dev_check
    - test -f .pipe-passed && STATE=SUCCESSFUL || STATE=FAILED
    - *post_build_status

################################################################################
##
## Dirac-like runner
##
## Please keep all "resource_group" jobs early in the order.
## In particluar, the server appears to grant the resource at random.
## This may cause the otherwise-in-order scheduling to ignore all such jobs
## until the *last* job from each group reaches its turn in the order.

.dirac:
  tags: [ shell, lbnl, class, dirac ]
  extends: [ .dev_ci, .server_socks ]
  variables:
    CI_MAKE_PARALLEL: "-j4"
    MY_OS:   'linux'
    MY_CPU:  'x86_64'
    MY_HOST: 'dirac'
    GASNET_SPAWNFN: L
    GASNET_SUPERNODE_MAXSIZE: 2
    UPCXX_HAVE_STATIC_LIBS_CCS_SMP: ''
    UPCXX_HAVE_STATIC_LIBS_CCS_UDP: ''
    CONFIGURE_ARGS: '--with-python=python3'

dirac-clang:
  extends: [ .new_llvm, .dirac ]
  before_script:
    - module load gmake/newest
    - module load clang
    - export CC=clang CXX=clang++

dirac-gcc:
  extends: [ .new_gnu, .dirac ]
  before_script:
    - module load gcc
    - export CC=gcc CXX=g++

dirac-pgi:
  extends: [ .new_pgi, .dirac ]
  resource_group: pgi_license # see note above regarding resource_group jobs
  before_script:
    - module load pgi
    - export CC=pgcc CXX=pgc++

dirac-pgi_floor:
  extends: [ .old_pgi, .dirac ]
  resource_group: pgi_license # see note above regarding resource_group jobs
  variables:
    CONFIGURE_ARGS: '--with-python=python2.7'
  before_script:
    - module load pgi/19.3
    - export CC=pgcc CXX=pgc++
    - export UPCXX_PLATFORM_HAS_ISSUE_390="${UPCXX_PLATFORM_HAS_ISSUE_390-1}"

dirac-intel_floor:
  extends: [ .old_intel, .dirac ]
  variables:
    CONFIGURE_ARGS: '--with-python=python2.7'
    CI_SHELL: '/usr/local/pkg/bash/3.2.57/bin/bash'
  resource_group: intel_license # see note above regarding resource_group jobs
  before_script:
    - module load intel/2017.2.174
    # Following "adapts" old Intel compilers to a "too new" glibc
    - export CC="icc -D__PURE_INTEL_C99_HEADERS__"
    - export CXX="icpc -D__PURE_INTEL_C99_HEADERS__"

dirac-intel:
  extends: [ .new_intel, .dirac ]
  variables:
    CI_SHELL: '/usr/local/pkg/bash/5.2.15/bin/bash'
  resource_group: intel_license # see note above regarding resource_group jobs
  before_script:
    - module load intel
    - export CC=icc CXX=icpc

dirac-aocc:
  extends: [ .new_aocc, .dirac ]
  before_script:
    - module load aocc
    - export CC=clang CXX=clang++

dirac-oneapi:
  extends: [ .new_oneapi, .dirac ]
  before_script:
    - module load oneapi
    - export CC=icx CXX=icpx

dirac-nvhpc:
  extends: [ .new_nvhpc, .dirac ]
  before_script:
    - module load nvhpc
    - export CC=nvc CXX=nvc++

# Valgrind tester (only smp/par/debug)
# Follows GASNet-EX develop by default
dirac-gcc_valgrind:
  extends: [ .new_gnu, .dirac ]
  variables:
    GASNET: 'https://bitbucket.org/berkeleylab/gasnet/get/develop.tar.gz'
  before_script:
    - module load gcc
    - module load valgrind
    - unset GASNET_SUPERNODE_MAXSIZE
    - export MORE_NO_TESTS='neg-'
    - export CONFIGURE_ARGS='--with-python=python3 --enable-valgrind --disable-auto-conduit-detect --enable-smp'
    - export CI_SINGLE=debug CI_NETWORKS=smp CI_SUBSETS=par-debug
    - export UPCXX_VERBOSE=1 UPCXX_OVERSUBSCRIBED=1 OMP_NUM_THREADS=1
    - export TEST_FLAGS_ISSUE138=-DMINIMAL TEST_ARGS_PERSONA_EXAMPLE=100
    - echo 'RUN_WRAPPER = $(TIMEOUT) -k 5m 10m $(VALGRIND_WRAPPER)' >> bld/Makefile.tests

# Compile-only tester with extended compiler checks,
# but a reduced set of tests (since many are not -Wall-clean).
# Note: `-DUPCXX_USE_NODISCARD=0` prevents complaints that `[[nodiscard]]` requires C++17.
# Note: `-Wno-unused -Wunused-result` is because clang is order-dependent (`-Wall`
#       overrides the same appearing earlier in the GASNet configure-probed flags).
dirac-clang_wall:
  extends: [ .new_llvm, .dirac ]
  before_script:
    - module load clang
    - export CC=clang CXX=clang++
    - export CI_RUN_TESTS=0
    - export MORE_NO_TESTS="nodiscard,issue"
    - export MANUAL_CXXFLAGS="-Werror -pedantic -Wall -Wno-unused -Wunused-result -DUPCXX_USE_NODISCARD=0"
    - export CI_EXTRAFLAGS+=" $MANUAL_CXXFLAGS"

# Compile-only tester with extended compiler checks,
# but a reduced set of tests (since many are not -Wall-clean).
dirac-gcc_wall:
  extends: [ .new_gnu, .dirac ]
  before_script:
    - module load gcc
    - export CC="gcc -fpic" CXX="g++ -fpic"
    - export CI_RUN_TESTS=0
    - export MORE_NO_TESTS="nodiscard,issue"
    - export MANUAL_CXXFLAGS="-Werror -pedantic -Wall"
    - export CI_EXTRAFLAGS+=" $MANUAL_CXXFLAGS"
    - export UPCXX_LIB_FPIC=1

# Compile-only tester with LTO and pedantic warnings enabled
# NOTE: only SMP because amudp and ammpi libs lead to harmless ODR warnings
dirac-gcc_lto:
  extends: [ .new_gnu, .dirac ]
  resource_group: lto_tester
  variables:
    CI_NETWORKS: "smp"
  before_script:
    - module load gcc
    - export CC="gcc -flto=auto -fuse-linker-plugin -ffat-lto-objects"
    - export CXX="g++ -flto=auto -fuse-linker-plugin -ffat-lto-objects"
    - export CI_EXTRAFLAGS="$CI_EXTRAFLAGS -pedantic"
    - export CI_RUN_TESTS=0
    - export CI_SINGLE=opt

dirac-oneapi_floor:
  extends: [ .old_oneapi, .dirac ]
  variables:
    CONFIGURE_ARGS: '--with-python=python2.7'
  before_script:
    - module load oneapi/2021.1.2
    - export CC=icx CXX=icpx

dirac-clang_floor:
  extends: [ .old_llvm, .dirac ]
  variables:
    CONFIGURE_ARGS: '--with-python=python2.7'
  before_script:
    - module load gmake/3.80
    - module load clang/4.0.0-gcc640
    - export CC=clang CXX=clang++

dirac-gcc_floor:
  extends: [ .old_gnu, .dirac ]
  variables:
    CONFIGURE_ARGS: '--with-python=python2.7'
  before_script:
    - module load gcc/6.4.0
    - export CC=gcc CXX=g++

dirac-aocc_old:
  extends: [ .old_aocc, .dirac ]
  before_script:
    - module load aocc/2.3.0
    - export CC=clang CXX=clang++

dirac-nvhpc_floor:
  extends: [ .old_nvhpc, .dirac ]
  variables:
    CONFIGURE_ARGS: '--with-python=python2.7'
  before_script:
    - module load nvhpc/20.9
    - export CC=nvc CXX=nvc++

################################################################################
##
## VM with current versions of macOS and Xcode
##

.macos_current:
  tags: [ shell, lbnl, class, xcode-latest ]
  extends: [ .dev_ci, .server_socks ]
  variables:
    CI_MAKE_PARALLEL: "-j4"
    MY_OS:   'macos'
    MY_CPU:  'x86_64'
    MY_HOST: 'macos_current'
    GASNET_SPAWNFN: L
    GASNET_SUPERNODE_MAXSIZE: 2
    UPCXX_RUN_TIME_LIMIT: 360
    UPCXX_HAVE_STATIC_LIBS_CCS_SMP: ''
    UPCXX_HAVE_STATIC_LIBS_CCS_UDP: ''
    UPCXX_LIB_FPIC: 1
    CONFIGURE_ARGS: '--with-python=/usr/local/bin/python3'

# NOTE: this job uses ssh-based spawn of udp-conduit for additional coverage
macos_current-xcode:
  extends: [ .new_xcode, .macos_current ]
  variables:
    GASNET_SPAWNFN: S
    GASNET_SSH_SERVERS: 'localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost'
    ci_script: macos-xcode

macos_current-gcc:
  extends: [ .new_gnu, .macos_current ]
  variables:
    CC:  gcc-12
    CXX: g++-12

# Compile-only tester with extended compiler checks,
# but a reduced set of tests (since many are not -Wall-clean).
# Note: `-DUPCXX_USE_NODISCARD=0` prevents complaints that `[[nodiscard]]` requires C++17.
# Note: `-std=c++11` prevents complaints about `long long` (in udp-conduit) and
#       empty macro arguments (in a probe in gasnet_macros.sh), both due to the
#       default C++98 in effect when those are compiled.
# Note: `-Wno-unused -Wunused-result` is because clang is order-dependent (`-Wall`
#       overrides the same appearing earlier in the GASNet configure-probed flags).
# Note: `-Wno-deprecated-declarations` is because Xcode 14+ has the delusion that
#       sprintf() is deprecated unless one demands a POSIX or XOPEN environment.
# Explicit `-fpic` even though this is macOS's default

macos_current-xcode_wall:
  extends: [ .new_xcode, .macos_current ]
  before_script:
    - export CC="clang -fpic" CXX="clang++ -fpic"
    - export CI_RUN_TESTS=0
    - export MORE_NO_TESTS="nodiscard,issue"
    - export MANUAL_CXXFLAGS="-Werror -std=c++11 -pedantic -Wall -Wno-unused -Wunused-result -Wno-deprecated-declarations -DUPCXX_USE_NODISCARD=0"
    - export CI_EXTRAFLAGS+=" $MANUAL_CXXFLAGS"

################################################################################
##
## VM with "floor" versions of macOS and Xcode
##

.macos_floor:
  tags: [ shell, lbnl, class, xcode-floor ]
  extends: [ .dev_ci, .server_socks ]
  variables:
    CI_MAKE_PARALLEL: "-j4"
    MY_OS:   'macos'
    MY_CPU:  'x86_64'
    MY_HOST: 'macos_floor'
    GASNET_SPAWNFN: L
    GASNET_SUPERNODE_MAXSIZE: 2
    UPCXX_RUN_TIME_LIMIT: 420
    UPCXX_HAVE_STATIC_LIBS_CCS_SMP: ''
    UPCXX_HAVE_STATIC_LIBS_CCS_UDP: ''
    UPCXX_LIB_FPIC: 1

macos_floor-xcode_floor:
  extends: [ .old_xcode, .macos_floor ]
  variables:
    ci_script: macos-xcode_old

# Is actually gcc 6.5.0 where 6.4.0 is our documented floor
macos_floor-gcc_floor:
  extends: [ .old_gnu, .macos_floor ]
  variables:
    CC:  gcc-6
    CXX: g++-6

################################################################################
##
## Power9 VM runner
##

.power9:
  tags: [ shell, lbnl, class, power9 ]
  extends: [ .dev_ci, .server_socks ]
  variables:
    CI_MAKE_PARALLEL: "-j4"
    MY_OS:   'linux'
    MY_CPU:  'ppc64le'
    MY_HOST: 'power9'
    GASNET_SPAWNFN: L
    GASNET_SUPERNODE_MAXSIZE: 2
    CONFIGURE_ARGS: '--enable-force-posix-realtime --with-python=python3'
    UPCXX_OVERSUBSCRIBED: 1

power9-gcc:
  extends: [ .new_gnu, .power9 ]
  before_script:
    - module load gcc
    - export CC=gcc CXX=g++

power9-clang:
  extends: [ .new_llvm, .power9 ]
  before_script:
    - module load clang
    - export CC=clang CXX=clang++

power9-nvhpc:
  extends: [ .new_nvhpc, .power9 ]
  before_script:
    - module load nvhpc
    - export CC=nvc CXX=nvc++

power9-gcc_floor:
  extends: [ .old_gnu, .power9 ]
  before_script:
    - module load gcc/6.4.0
    - export CC=gcc CXX=g++

power9-clang_floor:
  extends: [ .old_llvm, .power9 ]
  before_script:
    - module load clang/5.0.0-gcc640
    - export CC=clang CXX=clang++

power9-nvhpc_floor:
  extends: [ .old_nvhpc, .power9 ]
  before_script:
    - module load nvhpc/20.9
    - export CC=nvc CXX=nvc++

################################################################################
##
## AARCH64 runner (wombat)
##
## Host-specific note:
##  There is no unattended gitlab-runner on this system.
##  So, these jobs do NOT run except by request (match to CI_{HOST,NAME}).

.wombat:
  tags: [ shell, lbnl, class, aarch64 ]
  extends: [ .dev_ci, .rules_manual, .server_socks ]
  variables:
    CI_MAKE_PARALLEL: "-j16"
    CI_NETWORKS: 'ibv'
    MY_OS:   'linux'
    MY_CPU:  'aarch64'
    MY_HOST: 'wombat'
    CONFIGURE_ARGS: '--with-python=python3'
    ci_script: 'wombat-gcc'

wombat-gcc:
  extends: [ .new_gnu, .wombat ]
  variables:
    DEV_CI_COMPILER: gcc/latest

wombat-clang:
  extends: [ .new_llvm, .wombat ]
  variables:
    DEV_CI_COMPILER: clang/latest

# TODO: remove UPCXX_INSTALL_NOCHECK if/when it become unnecessary
wombat-nvhpc:
  extends: [ .new_nvhpc, .wombat ]
  variables:
    DEV_CI_COMPILER: nvhpc/latest
    UPCXX_INSTALL_NOCHECK: 1

wombat-gcc_floor:
  extends: [ .old_gnu, .wombat ]
  variables:
    DEV_CI_COMPILER: gcc/6.4.0

wombat-clang_floor:
  extends: [ .old_llvm, .wombat ]
  variables:
    DEV_CI_COMPILER: clang/4.0.0-gcc640

# TODO: remove UPCXX_INSTALL_NOCHECK if/when it become unnecessary
wombat-nvhpc_floor:
  extends: [ .old_nvhpc, .wombat ]
  variables:
    DEV_CI_COMPILER: nvhpc/20.9
    UPCXX_INSTALL_NOCHECK: 1

# GPU-enabled variants of the above
# Note the use of explicit compiler versions is needed to match nvcc limits
wombat_gpu-gcc:
  extends: wombat-gcc
  variables: { ci_script: 'wombat_gpu-gcc', DEV_CI_COMPILER: 'gcc/11.4.0' }
wombat_gpu-clang:
  extends: wombat-clang
  variables: { ci_script: 'wombat_gpu-gcc', DEV_CI_COMPILER: 'clang/14.0.6-gcc1140' }
wombat_gpu-nvhpc:
  extends: wombat-nvhpc
  variables: { ci_script: 'wombat_gpu-gcc', DEV_CI_COMPILER: 'nvhpc/23.9' }
wombat_gpu-gcc_floor:   { extends: wombat-gcc_floor,   variables: { ci_script: 'wombat_gpu-gcc' } }
wombat_gpu-clang_floor: { extends: wombat-clang_floor, variables: { ci_script: 'wombat_gpu-gcc' } }
wombat_gpu-nvhpc_floor: { extends: wombat-nvhpc_floor, variables: { ci_script: 'wombat_gpu-gcc' } }

################################################################################
##
## Perlmutter Phase 1 (GPU) compute nodes
##
## Host-specific note:
##  There is no unattended gitlab-runner on this system.
##  So, these jobs do NOT run except by request (match to CI_{HOST,NAME}).

.perlmutter.compute:
  tags: [ shell, nersc, perlmutter, user_hargrove ]
  extends: [ .dev_ci, .rules_manual, .server_socks ]
  variables:
    ci_script: perlmutter-nvidia
    CI_MAKE_PARALLEL: "-j8"
    CI_NETWORKS: "ofi"
    MY_OS:   'cnl'
    MY_CPU:  'x86_64'
    MY_HOST: 'perlmutter'
    CONFIGURE_ARGS: '--with-python=python3'

# Perlmutter PrgEnv_nvidia
perlmutter-PrgEnv_nvidia:
  extends: [ .new_nvhpc, .perlmutter.compute ]
  variables:
    DEV_CI_PRGENV: nvidia
    DEV_CI_COMPILER: nvidia/latest

# Perlmutter PrgEnv_gnu
perlmutter-PrgEnv_gnu:
  extends: [ .new_gnu, .perlmutter.compute ]
  variables:
    DEV_CI_PRGENV: gnu
#   DEV_CI_COMPILER: unset to preserve default for the PrgEnv

# Perlmutter PrgEnv_cray
perlmutter-PrgEnv_cray:
  extends: [ .new_cray, .perlmutter.compute ]
  variables:
    DEV_CI_PRGENV: cray
#   DEV_CI_COMPILER: unset to preserve default for the PrgEnv

# Perlmutter PrgEnv_aocc
perlmutter-PrgEnv_aocc:
  extends: [ .new_aocc, .perlmutter.compute ]
  variables:
    DEV_CI_PRGENV: aocc
#   DEV_CI_COMPILER: unset to preserve default for the PrgEnv

# Perlmutter PrgEnv_intel
perlmutter-PrgEnv_intel:
  extends: [ .new_intel, .perlmutter.compute ]
  variables:
    DEV_CI_PRGENV: intel
#   DEV_CI_COMPILER: unset to preserve default for the PrgEnv

##
## CI_GROUP:
## Used for "sweeps"
##

# CI_GROUP: wombat-gcc_exhaustive
#   All gcc compilers available on wombat
# CI_GROUP: wombat-gcc_all
#   Subset of wombat-gcc_exhaustive, which is intended to be useful but less expensive
.wombat-gcc_exhaustive:
  extends: [ .wombat, .rules_group ]
  variables: { MY_GROUP: 'wombat-gcc_exhaustive' }
.wombat-gcc_all:
  extends: [ .wombat-gcc_exhaustive ]
  variables: { MY_GROUP_2: 'wombat-gcc_all' }
_wombat-gcc_6_4_0:  { extends: .wombat-gcc_all,        variables: { DEV_CI_COMPILER: gcc/6.4.0  } }
_wombat-gcc_7_5_0:  { extends: .wombat-gcc_exhaustive, variables: { DEV_CI_COMPILER: gcc/7.5.0  } }
_wombat-gcc_8_5_0:  { extends: .wombat-gcc_all,        variables: { DEV_CI_COMPILER: gcc/8.5.0  } }
_wombat-gcc_9_5_0:  { extends: .wombat-gcc_exhaustive, variables: { DEV_CI_COMPILER: gcc/9.5.0  } }
_wombat-gcc_10_5_0: { extends: .wombat-gcc_all,        variables: { DEV_CI_COMPILER: gcc/10.5.0 } }
_wombat-gcc_11_4_0: { extends: .wombat-gcc_exhaustive, variables: { DEV_CI_COMPILER: gcc/11.4.0 } }
_wombat-gcc_12_3_0: { extends: .wombat-gcc_all,        variables: { DEV_CI_COMPILER: gcc/12.3.0 } }
_wombat-gcc_13_2_0: { extends: .wombat-gcc_exhaustive, variables: { DEV_CI_COMPILER: gcc/13.2.0 } }
_wombat-gcc_14_1_0: { extends: .wombat-gcc_all,        variables: { DEV_CI_COMPILER: gcc/14.1.0 } }
_wombat-gcc_14_2_0: { extends: .wombat-gcc_all,        variables: { DEV_CI_COMPILER: gcc/14.2.0 } }

# CI_GROUP: wombat-clang_exhaustive
#   All clang compilers available on wombat
# CI_GROUP: wombat-clang_all
#   Subset of wombat-clang_exhaustive, which is intended to be useful but less expensive
.wombat-clang_exhaustive:
  extends: [ .wombat, .rules_group ]
  variables: { MY_GROUP: 'wombat-clang_exhaustive' }
.wombat-clang_all:
  extends: [ .wombat-clang_exhaustive ]
  variables: { MY_GROUP_2: 'wombat-clang_all' }
_wombat-clang_4_0_0:  { extends: .wombat-clang_all,        variables: { DEV_CI_COMPILER: clang/4.0.0-gcc640   } }
_wombat-clang_5_0_1:  { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/5.0.1-gcc750   } }
_wombat-clang_6_0_1:  { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/6.0.1-gcc750   } }
_wombat-clang_7_0_1:  { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/7.0.1-gcc850   } }
_wombat-clang_7_1_0:  { extends: .wombat-clang_all,        variables: { DEV_CI_COMPILER: clang/7.1.0-gcc850   } }
_wombat-clang_8_0_0:  { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/8.0.0-gcc950   } }
_wombat-clang_9_0_1:  { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/9.0.1-gcc950   } }
_wombat-clang_10_0_1: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/10.0.1-gcc1140 } }
_wombat-clang_11_0_1: { extends: .wombat-clang_all,        variables: { DEV_CI_COMPILER: clang/11.0.1-gcc1140 } }
_wombat-clang_11_1_0: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/11.1.0-gcc1140 } }
_wombat-clang_12_0_1: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/12.0.1-gcc1140 } }
_wombat-clang_13_0_1: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/13.0.1-gcc1140 } }
_wombat-clang_14_0_6: { extends: .wombat-clang_all,        variables: { DEV_CI_COMPILER: clang/14.0.6-gcc1140 } }
_wombat-clang_15_0_7: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/15.0.7-gcc1140 } }
_wombat-clang_16_0_6: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/16.0.6-gcc1140 } }
_wombat-clang_17_0_6: { extends: .wombat-clang_exhaustive, variables: { DEV_CI_COMPILER: clang/17.0.6-gcc1140 } }
_wombat-clang_18_1_1: { extends: .wombat-clang_all,        variables: { DEV_CI_COMPILER: clang/18.1.1-gcc1140 } }

# CI_GROUP: wombat-nvhpc_exhaustive
#   All NVHPC compilers available on wombat
# CI_GROUP: wombat-nvhpc_all
#   Subset of wombat-nvhpc_exhaustive, which is intended to be useful but less expensive
# TODO: remove UPCXX_INSTALL_NOCHECK if/when it become unnecessary
.wombat-nvhpc_exhaustive:
  extends: [ .wombat, .rules_group ]
  variables:
    MY_GROUP: 'wombat-nvhpc_exhaustive'
    UPCXX_INSTALL_NOCHECK: 1
.wombat-nvhpc_all:
  extends: [ .wombat-nvhpc_exhaustive ]
  variables: { MY_GROUP_2: 'wombat-nvhpc_all' }
_wombat-nvhpc_20_9:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/20.9  } }
_wombat-nvhpc_20_11: { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/20.11 } }
_wombat-nvhpc_21_1:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/21.1  } }
_wombat-nvhpc_21_2:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/21.2  } }
_wombat-nvhpc_21_3:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/21.3  } }
_wombat-nvhpc_21_5:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/21.5  } }
_wombat-nvhpc_21_7:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/21.7  } }
_wombat-nvhpc_21_9:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/21.9  } }
_wombat-nvhpc_21_11: { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/21.11 } }
_wombat-nvhpc_22_1:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/22.1  } }
_wombat-nvhpc_22_2:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/22.2  } }
_wombat-nvhpc_22_3:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/22.3  } }
_wombat-nvhpc_22_5:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/22.5  } }
_wombat-nvhpc_22_7:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/22.7  } }
_wombat-nvhpc_22_9:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/22.9  } }
_wombat-nvhpc_22_11: { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/22.11 } }
_wombat-nvhpc_23_1:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/23.1  } }
_wombat-nvhpc_23_3:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/23.3  } }
_wombat-nvhpc_23_5:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/23.5  } }
_wombat-nvhpc_23_7:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/23.7  } }
_wombat-nvhpc_23_9:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/23.9  } }
_wombat-nvhpc_23_11: { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/23.11 } }
_wombat-nvhpc_24_1:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/24.1  } }
_wombat-nvhpc_24_3:  { extends: .wombat-nvhpc_exhaustive, variables: { DEV_CI_COMPILER: nvhpc/24.3  } }
_wombat-nvhpc_24_5:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/24.5  } }
_wombat-nvhpc_24_7:  { extends: .wombat-nvhpc_all,        variables: { DEV_CI_COMPILER: nvhpc/24.7  } }
