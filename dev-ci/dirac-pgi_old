#!/bin/bash

## BOILERPLATE
source $(dirname $0)/_ci-common

## ENVIRONMENT
module purge
module load PrgEnv/pgi/19.1
module load cuda/9.0
module load gmake/3.80
export TMPDIR=/dev/shm
export GASNET_SPAWNFN=L
export GASNET_SUPERNODE_MAXSIZE=2
export UPCXX_HAVE_OPENMP=1

export NETWORKS="${CI_NETWORKS:-udp}"
export GMAKE="${CI_MAKE:-${GMAKE:-make}}"
MAKE_CMD="$GMAKE ${CI_MAKE_PARALLEL:--j16}"

export UPCXX_PLATFORM_HAS_ISSUE_390="${UPCXX_PLATFORM_HAS_ISSUE_390-1}"

## CONFIGURE
if [[ $STAGES =~ ,configure, ]]; then
  eval $CI_SHELL $UPCXX/configure ${GASNET:+ --with-gasnet=$GASNET} CC=pgcc CXX=pgc++ \
        --without-mpicc --with-ibv-physmem-max=2/3 --without-ibv-odp --with-cuda \
        ${CI_CONFIGURE_ARGS}
fi

## BUILD
if [[ $STAGES =~ ,build, ]]; then
  eval $DO_BUILD
fi

## TESTS
if [[ $STAGES =~ ,tests, ]]; then
  # PGI floor frontend ICEs when encountering too many debug symbols
  eval $DO_COMPILE || touch .pipe-fail
fi

## RUN
unset TMPDIR
if [[ $STAGES =~ ,run, ]]; then
  time $GMAKE $RUN_TARGET || touch .pipe-fail
fi

## DONE
finished
