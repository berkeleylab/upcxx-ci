#!/bin/bash

## BOILERPLATE
source $(dirname $0)/_ci-common

## ENVIRONMENT

## Some knobs to parameterize environment modules:
### DEV_CI_PRGENV    - one of cray, amd, gnu
### DEV_CI_CRAYPE    - optional space-seprated list of craype-* modules to load
### DEV_CI_COMPILER  - module/version (empty version for default, "latest" special cased)

module load PrgEnv-${DEV_CI_PRGENV:-cray} 2>/dev/null

if [[ -n "${DEV_CI_CRAYPE}" ]] ; then
  module load ${DEV_CI_CRAYPE}
fi

CC_MODULE=${DEV_CI_COMPILER}
CC_FAM=${CC_MODULE%%/*}

if [[ $CC_MODULE == ${CC_FAM}/latest ]]; then
  CC_MODULE=$(module -t avail ${CC_FAM}/ |& sort -rV | grep -m1 ^${CC_FAM})
fi
[[ -n "${CC_MODULE}" ]] && module load ${CC_MODULE}

# The `amd` module already includes ROCm paths and is mutually exclusive w/ 'rocm' module
[[ :${LOADEDMODULES} =~ :amd/ ]] || module load rocm

#export TMPDIR=$XDG_RUNTIME_DIR
export UPCXX_HAVE_OPENMP=1

export NETWORKS="${CI_NETWORKS:-ofi}"
export GMAKE="${CI_MAKE:-${GMAKE:-make}}"
MAKE_CMD="$GMAKE ${CI_MAKE_PARALLEL:--j8}"

# Network-independent settings
export PMIRUN_CMD="${PMIRUN_CMD:-srun -K0 -W60 %V -mblock,NoPack -n %N -- %C}"
export GASNET_CONFIGURE_ARGS+=' --disable-ibv'
export CI_CONFIGURE_ARGS="--enable-hip $CI_CONFIGURE_ARGS"

SLURM_COMMON='-A CSC296_crusher'

# Job topology for network conduit
#
# Default layout is 2 nodes x 2 ppn.
# Each process is bound to the first 7 cores of a NUMA node (with the
# second thread of each core idle) and the two GPUs in that NUMA node.
# Note that 1 core (2 threads) of each NUMA node is reserved by default.
# Placing processes in distinct NUMA nodes allows ofi-conduit to use a distinct
# NIC for each.
RANKS=${RANKS:-4}
NODES=${NODES:-2}
NODES=$(( NODES > RANKS ? RANKS : NODES ))
CORES=${CORES:-14} # CPU cores per process
# Note 1: CPU binding
#  Slurm on Crusher is configured to bind to cores by default, and the
#  -c/--cpus-per-task operate in units of cores.  So, no options beyond
#  `-c$CORES` (or the environment equivalent) are required.
# Note 2: GPU binding
#  We need arguments which account for all eight GPUs, or Slurm gets binding
#  wrong.  So, even though the following yields `--gpus-per-task=4` (not 2) in
#  the default case, the actual number will be reduced by the "closest" binding
#  to just two.  Using --gpus-per-task=2 would trigger a Slurm misbehavior.
PPN=$(( RANKS / NODES ))
SLURM_OPTS_NET="$SLURM_COMMON -N$NODES -n$RANKS --gpus-per-task=$((8/PPN)) --gpu-bind=closest"
SLURM_ENV_NET="SRUN_CPUS_PER_TASK=$CORES"  # Value of -c[N] passed to salloc is ignored

# Job topology for smp-conduit
#
# All 56 (non-reserved) cores and 8 GPUs are made available to the single process.
# The second h/w thread of each core is unused.
SLURM_OPTS_SMP="$SLURM_COMMON -N1 -n1 -c56 --gpus-per-task=8"
SLURM_ENV_SMP=""

# Network-specific settings
if [[ $NETWORKS =~ ofi ]]; then
  # Not enabled by default in GASNet stable
  export FI_PROVIDER="${FI_PROVIDER:-cxi}"
  export GASNET_CONFIGURE_ARGS+=' --enable-ofi --with-ofi-spawner=pmi'
  export GASNET_CONFIGURE_ARGS+=" --with-ofi-provider='$FI_PROVIDER'"
  # Bind proceses to nearest NIC based on their NUMA node
  export GASNET_OFI_DEVICE_TYPE=Node
  export GASNET_OFI_DEVICE_0=cxi2
  export GASNET_OFI_DEVICE_1=cxi1
  export GASNET_OFI_DEVICE_2=cxi3
  export GASNET_OFI_DEVICE_3=cxi0
fi
if [[ $NETWORKS =~ ucx ]]; then
  # Not enabled by default in GASNet stable
  # Also not in default paths (network-ofi and network-ucx modules are mutually exclusive)
  export GASNET_CONFIGURE_ARGS+=' --enable-ucx -enable-ucx-rpath --with-ucx-spawner=pmi'
  export UCX_HOME="${UCX_HOME:-$(find /opt/cray/pe/ucx -maxdepth 1|sort -V|tail -1)/ucx}"
  export UCX_HANDLE_ERRORS="${UCX_HANDLE_ERRORS-}"  # default to empty string
  export UCX_TLS="${UCX_TLS-tcp,self,cma,posix,sysv}"
fi
if [[ $NETWORKS =~ udp ]]; then
  export GASNET_CSPAWN_CMD="${GASNET_CSPAWN_CMD:-${PMIRUN_CMD/%V}}"
  export GASNET_SPAWNFN=C
fi

## CONFIGURE
if [[ $STAGES =~ ,configure, ]]; then
  eval $CI_SHELL $UPCXX/configure ${GASNET:+ --with-gasnet=$GASNET} CC=cc CXX=CC ${CI_CONFIGURE_ARGS}
fi

## BUILD
if [[ $STAGES =~ ,build, ]]; then
  eval $DO_BUILD
fi

## TESTS
if [[ $STAGES =~ ,tests, ]]; then
  eval $DO_COMPILE || ( touch .pipe-fail; echo FAILED to compile tests )
fi

## RUN
if [[ $STAGES =~ ,run, ]]; then
  unset TMPDIR
  if [[ $NETWORKS =~ smp ]]; then
    time srun $SLURM_OPTS_SMP -t 45 -- sh -c \
        "time env $SLURM_ENV_SMP $GMAKE $RUN_TARGET NETWORKS=smp" || \
      ( touch .pipe-fail; echo Batch job to run smp tests FAILED )
  fi
  for net in ${NETWORKS//,/ }; do
    [[ $net = smp ]] && continue;
    if [[ $net = udp ]] ; then minutes=120; else minutes=90; fi
    time salloc $SLURM_OPTS_NET -t $minutes -- sh -c \
	"time env $SLURM_ENV_NET $GMAKE $RUN_TARGET NETWORKS=$net" || \
      ( touch .pipe-fail; echo Batch job to run $net tests FAILED )
  done
fi

## DONE
finished
