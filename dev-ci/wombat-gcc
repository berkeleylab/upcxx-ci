#!/bin/bash

## BOILERPLATE
source $(dirname $0)/_ci-common

PKG=/proj/phargrov/wombat/pkg

## ENVIRONMENT

# DEV_CI_COMPILER - family[/version] where "latest" is a special-cased version

CC_MODULE=${DEV_CI_COMPILER:-gcc/latest}
CC_FAM=${CC_MODULE%%/*}

module purge || module purge  # something odd
module use /sw/wombat/modulefiles
module use $PKG/modulefiles

if [[ $CC_MODULE == ${CC_FAM}/latest ]]; then
  CC_MODULE=$(module -t avail ${CC_FAM}/ 2>&1 | grep "^${CC_FAM}/[0-9]" | sed -e 's/(.*)//' | sort -V | tail -1)
fi
module load ${CC_MODULE} ${DEV_CI_EXTRA_MODULES}

export TMPDIR=$XDG_RUNTIME_DIR
export GASNET_SUPERNODE_MAXSIZE=2
export UPCXX_HAVE_OPENMP=1

export NETWORKS="${CI_NETWORKS:-ibv}"
export GMAKE="${CI_MAKE:-${GMAKE:-make}}"
MAKE_CMD="$GMAKE ${CI_MAKE_PARALLEL:--j16}"

export GASNET_MASTERIP=$(ip addr show | grep -o "172.30.140.[^ /]*" | head -1)
export GASNET_WORKERIP="172.30.140.128"

SLURM_OPTS="${SLURM_OPTS:- -p A64fx}"
SRUN_CMD="${SRUN_CMD:-srun -K0 -W60 %V -n %N %C}"

# Uses system-provided PMIx, but we need to provide the headers
export SLURM_MPI_TYPE='pmix_v3'

# Network-specific settings
if [[ $NETWORKS =~ udp ]]; then
  # Cannot get proper GPU assignment (when applicable) if spawned via SSH
  export GASNET_SPAWNFN=C
  export GASNET_CSPAWN_CMD="$SRUN_CMD"
fi
if [[ $NETWORKS =~ ibv ]]; then
  CI_CONFIGURE_ARGS+=' --with-ibv-spawner=pmi'
fi
if [[ $NETWORKS =~ ofi ]]; then
  CI_CONFIGURE_ARGS+=' --enable-ofi --enable-ofi-rpath --with-ofi-spawner=pmi'
  CI_CONFIGURE_ARGS+=" --with-ofi-home=${OFI_HOME:-$PKG/libfabric/1.21.0}"
fi
if [[ $NETWORKS =~ ucx ]]; then
  CI_CONFIGURE_ARGS+=' --enable-ucx --enable-ucx-rpath --with-ucx-spawner=pmi'
  CI_CONFIGURE_ARGS+=" --with-ucx-home=${UCX_HOME:-$PKG/ucx/1.15.0}"
  unset UCX_HOME # avoids unused env var warnings
fi
if [[ $NETWORKS =~ mpi ]]; then
  module load mpi
  [[ -n $CC  ]] && export MPICH_CC=$CC
  [[ -n $CXX ]] && export MPICH_CXX=$CXX
  [[ -n $FC  ]] && export MPICH_FC=$FC
  #module load openmpi
  #[[ -n $CC  ]] && export OMPI_CC=$CC
  #[[ -n $CXX ]] && export OMPI_CXX=$CXX
  #[[ -n $FC  ]] && export OMPI_FC=$FC
  #export OMPI_MCA_opal_warn_on_missing_libcuda=0
  export CXX=mpicxx # hack!
fi

## CONFIGURE
if [[ $STAGES =~ ,configure, ]]; then
  eval $CI_SHELL $UPCXX/configure ${GASNET:+ --with-gasnet=$GASNET} \
        --with-ibv-physmem-max=2/3 --disable-ibv-odp \
        --with-ibv-max-hcas=2 --with-ibv-ports=mlx5_0+mlx5_3 \
        --enable-pmi --with-pmi-version=x --with-pmirun-cmd=\"$SRUN_CMD\" \
        --disable-mpi-compat \
        ${CI_CONFIGURE_ARGS}
fi

## BUILD
if [[ $STAGES =~ ,build, ]]; then
  eval $DO_BUILD
fi

## TESTS
if [[ $STAGES =~ ,tests, ]]; then
  eval $DO_COMPILE || ( touch .pipe-fail; echo FAILED to compile tests )
fi

## RUN
unset TMPDIR
if [[ $STAGES =~ ,run, ]]; then
  date
  if [[ $NETWORKS =~ smp ]]; then
    time srun $SLURM_OPTS -t 90 -N1 -n1 -- bash -c \
        "date; time $GMAKE $RUN_TARGET NETWORKS=smp" || \
        ( touch .pipe-fail; echo Batch job to run smp tests FAILED )
  fi
  for net in ${NETWORKS//,/ }; do
    [[ $net = smp ]] && continue;
    time salloc $SLURM_OPTS -t 90 -N2 -- bash -c \
        "date; time $GMAKE $RUN_TARGET NETWORKS=$net" || \
        ( touch .pipe-fail; echo Batch job to run $net tests FAILED )
  done
fi

## DONE
finished
